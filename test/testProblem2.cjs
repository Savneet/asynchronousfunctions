const readDataProblem=require('../problem2.cjs');

process.chdir(__dirname + '/..') // Changing the current working directory to parent directory. So as to get the paths relative to that directory

try {
    readDataProblem();
} catch (error) {
    console.log(error);
}
