const fs = require("fs");

function readData() {
  fs.readFile("./lipsum_1.txt", "UTF-8", (err, data) => {
    if (err) {
      console.log("Error");
    } else {
      console.log("File read");
      fs.writeFile("./upper.txt", data.toUpperCase(), (err) => {
        if (err) {
          console.log(err);
        } else {
          console.log("File created");
          fs.writeFile("./filenames.txt", "upper.txt\n", (err) => {
            if (err) {
              console.log(err);
            } else {
              fs.readFile("./upper.txt", "utf-8", (err, data) => {
                if (err) {
                  console.log(err);
                } else {
                  let d = data.toLowerCase();
                  let splitData = d.split(".").join("\n");
                  fs.writeFile("./lowerAndSplit.txt", splitData, (err) => {
                    if (err) {
                      console.log(err);
                    } else {
                      fs.appendFile(
                        "./filenames.txt",
                        "lowerAndSplit.txt\n",
                        (err) => {
                          if (err) {
                            console.log(err);
                          } else {
                            fs.readFile(
                              "./lowerAndSplit.txt",
                              "utf-8",
                              (err, data) => {
                                if (err) {
                                  console.log(err);
                                } else {
                                  let sortedData = data
                                    .trim()
                                    .split("\n")
                                    .sort()
                                    .toString();
                                  fs.writeFile(
                                    "./sortedData.txt",
                                    sortedData,
                                    (err) => {
                                      if (err) {
                                        console.log(err);
                                      } else {
                                        fs.appendFile(
                                          "./filenames.txt",
                                          "sortedData.txt\n",
                                          (err) => {
                                            if (err) {
                                              console.log(err);
                                            } else {
                                              fs.readFile(
                                                "./filenames.txt",
                                                "utf-8",
                                                (err, data) => {
                                                  data
                                                    .split("\n").filter(element => element.trim() !== "")
                                                    .forEach((element) => {
                                                      fs.unlink(
                                                        `./${element}`,
                                                        (err) => {
                                                          if (err) {
                                                            console.log(err);
                                                          } else {
                                                            console.log("Done");
                                                          }
                                                        }
                                                      );
                                                    });
                                                }
                                              );
                                            }
                                          }
                                        );
                                      }
                                    }
                                  );
                                }
                              }
                            );
                          }
                        }
                      );
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}

module.exports = readData;

// fs.writeFile('./filenames.txt',d.toUpperCase(),(err)=>{
//     if(err)
//     {
//         console.log(err);
//     }
//     else
//     {
//         console.log(data);
//     }
// });
