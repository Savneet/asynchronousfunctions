const fs = require("fs");
const path = require("path");

const directoryPath = path.join(__dirname, "jsonFiles");

function createAndDeleteRandomFiles()
{
  fs.mkdir(directoryPath, (err) => {
    if (err) {
      console.log(err);
    } else {
      console.log("Directory created ");
      const numFiles = 5;
      let count = 0;
      for (let index = 0; index < numFiles; index++) {
        const fileName = `file${index}.json`;
        const filePath = path.join(directoryPath, fileName);
        const jsonData = JSON.stringify({ name: "Savneet", gender: "female" });
  
        fs.writeFile(filePath, jsonData, (err) => {
          if (err) {
            console.log(err);
          } else {
            console.log("File created successfully");
            count++;
            if (count == numFiles) {
              //All files created, now start deleting them
              for (let index = 0; index < numFiles; index++) {
                const fileName = `file${index}.json`;
                const filePath = path.join(directoryPath, fileName);
                fs.unlink(filePath, (err) => {
                  if (err) {
                    console.log(err);
                  } else {
                    console.log("File Deleted successfully");
                  }
                });
              }
            }
          }
        });
      }
    }
  });  
}

module.exports=createAndDeleteRandomFiles;